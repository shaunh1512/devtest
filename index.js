var express = require("express");
var app = express();

app.controllers = require("./server/controllers/")(app);

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/client/index.html");
});

app.listen("8888", function() {
  console.log("All gravy on port 8888");
});
